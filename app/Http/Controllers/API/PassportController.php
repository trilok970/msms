<?php

namespace App\Http\Controllers\API;
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");
// following files need to be included

require_once  "paytm/lib/config_paytm.php";
require_once  "paytm/lib/encdec_paytm.php";

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Passport;
use App\Models\User;
use DB;
use Hash;
use File;
use Str;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMail;
use Auth;
use App\Models\Channel;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\DeviceToken;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Banner;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderCancel;
use App\Models\OrderShipping;
use App\Models\OrderDelivery;
use App\Models\OtherAddress;
use App\Models\TimeSlot;
use App\Models\City;
use App\Models\State;
use App\Models\PromoCode;

class PassportController extends Controller
{
    //
	 public $successStatus = 200;
	 public $failureStatus = 401;

	public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
    }
	public function sendResponse($result, $message)
    {
    	$response = [
            'status' => true,
			'message' => $message,
            'data'    => $result,

        ];


        return response()->json($response, 200);
    }
     /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
    	$response = [
            'status' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    private function check_phone_number($phone_number)
    {
		return $user = User::where(["phone_number"=>$phone_number,"is_deleted"=>0])->count();
	}
	private function check_email($email)
    {
		return $user = User::where(["email"=>$email,"is_deleted"=>0])->count();
	}
	public function check_device_token($user_id,$device_token,$device_type)
	{
		$count = DeviceToken::where(['device_type'=>$device_type,'device_token'=>$device_token])->count();
		if($count == 0)
		{
			$devicetoken = new DeviceToken;
			$devicetoken->user_id = $user_id;
			$devicetoken->device_token = $device_token;
			$devicetoken->device_type = $device_type;
			$devicetoken->save();
		}



	}
	public function delete_device_token($user_id,$device_token)
	{
		$device_token = DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->first();
		if(!$device_token)
			DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->delete();



	}
	public function register(Request $request)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		$validator = Validator::make($request->all(),[
			'fullname' => 'required|max:255',
			'email' => 'required|email',
            'password' => 'required',
			'device_type'=>'required',
			'device_token'=>'required',
			'phone_number'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}

			if($this->check_email($request->email) > 0)
			{
				$data['status'] = false;
				$data['message']= "The email already been taken.";
				return response()->json($data);
			}

			// if($this->check_phone_number($request->phone_number) > 0)
			// {
			// 	$data['status'] = false;
			// 	$data['message']= "The mobile no has already been taken.";
			// 	return response()->json($data);
			// }
			// if($request->hasFile('image') && $request->image->isValid())
   //         {
   //             $extension = $request->image->extension();
   //             $fileName  = time().".$extension";
   //             $request->image->move(public_path('images'),$fileName);
   //         }
   //         else
   //         {
   //             $fileName = "default.jpg";
   //         }


			$user = new User;
			$user->fullname = ucwords($request->fullname);
			$user->phone_number = $request->phone_number;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			// $user->device_type = $request->device_type ??  "NA";
			// $user->device_token = $request->device_token ??  "NA";




	try{
		if($user->save())
		{
			$this->check_device_token($user->id,$request->device_token,$request->device_type);
        $user['token'] =  $user->createToken('MyApp')->accessToken;
		// $admin = User::find(1);
		// $subject = "Welcome Message";
		// $admin_msg = ucwords($user->name)."'s registration has been done in B2 Studies App.";
		// $msg = "Welcome to B2 Studies<br> Your registration has been done in B2 Studies App<br>Your login credential is username : ".$user->phone_number." and password : ".$pass;
		// $this->send_user_email($admin->other_email,$admin_msg,$admin->name,$subject);
		// $this->send_user_email($user->email,$msg,$user->name,$subject);

		// $this->send_mobile_message_url($user->phone_number,$msg);
		// $this->send_mobile_message_url("7742223269",$admin_msg);

        return $this->sendResponse($user, 'User registered successfully');
		}
		}
		catch (\Exception $e) {
       dd($e);
    }

	}

	 public function login(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'email' => 'required|email',
            'password' => 'required',
			'device_type'=>'required',
			'device_token'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}

        $user = User::where(['email'=> $request->email,'is_deleted'=>0])->first();

        if($user)
        {
        	if($user->status==0)
	        {
	        	$data['status'] = false;
				$data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, $this->successStatus);
	        }
	        if($user->is_deleted==1)
	        {
	        	$data['status'] = false;
				$data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, 200);
	        }
			if (Hash::check($request->password, $user->password))
			{
				try{
				$token = $user->createToken('Laravel Password Grant Client')->accessToken;
				$user->remember_token = Str::random(100);
				$user->save();
			$this->check_device_token($user->id,$request->device_token,$request->device_type);


				$data['status'] = true;
				$data['message'] = 'Login Success';
				$path = url('/').'/public/images/'.'/';;
				$thumb = url('/').'/public/thumbnail/';
				$user['token'] =  $token;
                $user['path'] =  $path;
				$data['data'] =  $user;

				}
		     catch (\Exception $e) {
             dd($e);
			 }

				//$response['data'] =  $user;
				$msg="Dear ".$user->fullname."<br>You are successfully login";
				//$this->notification($user->device_token, "Login", $msg,"login");

			    return response()->json($data, $this->successStatus);
			}
			else
			{
				$data['status'] = false;
				$data['message'] = 'Email and password incorrect';
				$data['data'] = array();
			    return response()->json($data, $this->successStatus);
			}

    }
	else
	{
				$data['status'] = false;
				$data['message'] = 'User does not exist';
				$data['data'] = array();
			    return response()->json($data, $this->successStatus);
    }

	}
	 public function forgot_password(Request $request)
    {

        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

             $user = User::where("email",$request->email)->first();
             $data['status']  =  true;
             $data['message'] =  'Your password reset link sent to your registered e-mail id';


        if($user!=null)
        {

           // send sms to user
           // $digits = 6;
           // $pass = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        	$link = url('reset-password');
           $message = "Hello ".ucwords($user->fullname)."<br>Your Reset Password link is <br> $link";
          // $res = $this->send_mobile_message_url($user->mobile_no,$message);


           // $user->password = bcrypt($pass);
           // $user->save();

            return response()->json($data, $this->successStatus);
        }
        else{
             $response = "This e-mail id is not registered";
            return $this->sendError($response);
        }
    }
    public function reset(Request $request)
    {

        // $credentials = request()->validate([
        //     'email' => 'required|email',
        //     'token' => 'required|string',
        //     'password' => 'required|string|confirmed'
        // ]);
        $validator = Validator::make($request->all(),[
        'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|confirmed'
        ]);
        if($validator->fails())
        {


            return back()
            ->withInput()
            ->withErrors($validator);
        }
// echo "<pre>";
//     	dd(request());
//     	exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
        }

        return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
    }
    public function test_mail()
    {
    	 Mail::to('trilok_kumar@ninehertzindia.com')->send(new UserMail());
    }
    public function edit_profile(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'fullname' => 'required',
			'email' => 'required|email',
			'phone_number'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{

				if($request->email != $user->email)
				{
					if($this->check_email($request->email) > 0)
					{
						$data['status'] = false;
						$data['message']= "The email already been taken.";
						return response()->json($data);
					}
				}


				if($request->hasFile('profile_pic') && $request->profile_pic->isValid())
	           {

	               $extension = $request->profile_pic->extension();
	               $fileName  = "uploads/images/".time().".$extension";
	               $request->profile_pic->move(public_path('images'),$fileName);
	           }
	           else
	           {
	               $fileName = $user->profile_pic;
	           }


				$user->fullname = ucwords($request->fullname);
				$user->phone_number = $request->phone_number;
				$user->email = $request->email;
				$user->profile_pic = $fileName;
				if($user->save())
				{
					$user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User profile updated successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);
				}
			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
	             $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
     public function get_profile(Request $request)
    {

			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{
				 $user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User Detail Found Successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);

			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
				 $data['message'] = 'User Not Found';
		         return response()->json($data, $this->successStatus);
			}

    }
     public function change_password(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'old_password' => 'required',
			'new_password' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
             if($validator->errors()->first('userid'))
			 $data['message']	= $validator->errors()->first('userid');
             else if($validator->errors()->first('old_password'))
			 $data['message']	= $validator->errors()->first('old_password');
             else if($validator->errors()->first('new_password'))
			 $data['message']	= $validator->errors()->first('new_password');
              return response()->json($data);
			}
			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{
				 if(Hash::check($request->old_password,$user->password))
				 {
				 	$user->update(['password' => Hash::make($request->new_password)]);
				    $data['status']  =  true;
		            $data['message'] =  'Your password changed successfully';
			        return response()->json($data, $this->successStatus);
				 }
				 else
				 {
				 	 $data['status']  =  false;
		             $data['message'] =  'Old password does not match';
			         return response()->json($data, $this->successStatus);
				 }

			}
			else
			{
				 $data['status']  = false;
	             $data['message'] = 'User Not Found';
		         return response()->json($data, $this->successStatus);
			}

    }

    public function social_check(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'social_id' => 'required',
			'social_type' => 'required|in:FACEBOOK,GOOGLE,INSTAGRAM',
			// 'email' => 'required',
			// 'fullname' => 'required',
			// 'phone_number' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
             if($validator->errors()->first('social_id'))
			 $data['message']	= $validator->errors()->first('social_id');
             else if($validator->errors()->first('social_type'))
			 $data['message']	= $validator->errors()->first('social_type');
    //          else if($validator->errors()->first('email'))
			 // $data['message']	= $validator->errors()->first('email');
    //          else if($validator->errors()->first('fullname'))
			 // $data['message']	= $validator->errors()->first('fullname');
    //          else if($validator->errors()->first('phone_number'))
			 // $data['message']	= $validator->errors()->first('phone_number');
              return response()->json($data);
			}

			$user_count = User::where(['social_id'=>$request->social_id,'is_deleted'=>0])->count();

			$path = url('public/images/').'/';
			if($user_count > 0)
			{
				$user = User::where(['social_id'=>$request->social_id,'is_deleted'=>0])->first();

				if($user->status==0)
		        {
		        	$data['status'] = false;
					$data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
					$data['data'] = array();
					return response()->json($data, $this->successStatus);
		        }
		        if($user->is_deleted==1)
		        {
		        	$data['status'] = false;
					$data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
					$data['data'] = array();
					return response()->json($data, $this->successStatus);
		        }

				$token = $user->createToken('Laravel Password Grant Client')->accessToken;
				if($request->profile_pic)
				{
					$imgurl = $request->get('profile_pic');
	                $image_name = 'user_'.$user->id.'.jpeg';

	                $destinationPath = '/images/';
	                $ppath = public_path().'/'.$destinationPath;

	               file_put_contents($ppath.$image_name,file_get_contents($imgurl));
	            }
	            else
	            	$image_name = $user->profile_pic;

				$user->device_token = $request->device_token ?? "";
				$user->device_type = $request->device_type ?? "";
				$user->fullname 	= ucwords($request->fullname) ?? "";
				$user->device_type  = $request->device_type ??  "";
				$user->device_token = $request->device_token ??  "";
				$user->social_id    = $request->social_id ??  "";
				$user->social_type  = $request->social_type ??  "";
				$user->remember_token = Str::random(100);
				$user->profile_pic = $image_name;
				$user->save();

				$data['status'] = true;
				$data['message'] = 'Login Success';
				$data['data'][0] =  $user;
				$data['data'][0]['token'] =  $token;
				$data['data'][0]['path'] =  $path;


			    return response()->json($data, $this->successStatus);

			}
			else
			{
				if($this->check_email($request->email) > 0)
				$user = User::where(['email'=>$request->email,'is_deleted'=>0])->first();
				else
				$user = new User;

				if($request->phone_number && $this->check_phone_number($request->phone_number) > 0)
				$user = User::where(['phone_number'=>$request->phone_number,'is_deleted'=>0])->first();





				$user->fullname 	= ucwords($request->fullname) ?? "";
				$user->phone_number = $request->phone_number ?? "";
				$user->email 		= $request->email ?? "";
				$user->device_type  = $request->device_type ??  "";
				$user->device_token = $request->device_token ??  "";
				$user->social_id    = $request->social_id ??  "";
				$user->social_type  = $request->social_type ??  "";
				$user->remember_token = Str::random(100);
				$user->save();

				$user = User::find($user->id);
				if($request->profile_pic)
				{
					$imgurl = $request->get('profile_pic');
	                $image_name = 'user_'.$user->id.'.jpeg';

	                $destinationPath = '/images/';
	                $ppath = public_path().'/'.$destinationPath;

	               file_put_contents($ppath.$image_name,file_get_contents($imgurl));
	            }
	            else
	            	$image_name = $user->profile_pic ?? "user.jpg";

	            $user->profile_pic = $image_name;
	            $user->save();

				$token = $user->createToken('Laravel Password Grant Client')->accessToken;

				$data['status'] = true;
				$data['message'] = 'Login Success';
				$data['data'][0] =  $user;
				$data['data'][0]['token'] =  $token;
				$data['data'][0]['path'] =  $path;
				$msg="Dear ".$user->fullname."<br>You are successfully login";
				//$this->notification($user->device_token, "Login", $msg,"login");

			    return response()->json($data, $this->successStatus);
			}

    }
     public function logout(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'device_token' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$user = Auth::user();
			if($user)
			{
				$request->user()->token()->revoke();
				$this->delete_device_token($user->id,$request->device_token);
				$data['status'] = true;
				$data['message'] = 'Logout successfully';

			    return response()->json($data, $this->successStatus);
			}
			else
			{
				 $data['status']  = false;
				 $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
// Get all category
     public function categories(Request $request)
    {
		$path = url('public/images/').'/';

    	$category = Category::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Categories Found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Categories Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    // Get all subcategory
     public function subcategories(Request $request)
    {
        $path = url('public/images/').'/';
        if($request->category_id)
        $category_id = $request->category_id;
        else
        $category_id = '';

        $category = Subcategory::where(['is_deleted'=>0])
                    ->when($category_id,function($q,$category_id){
                        return $q->where('category_id',$category_id);
                    })
                    ->with('category')->orderByDesc('id')->get();
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Subcategories Found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Subcategories Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }

    // Get all prodcuts
    public function products(Request $request)
    {
		$path = url('public/images/').'/';

        if($request->category_id)
        $category_id = $request->category_id;
        else
        $category_id = '';

        if($request->subcategory_id)
        $subcategory_id = $request->subcategory_id;
        else
        $subcategory_id = '';

        $search = trim($request->search);


        $products = Product::select('products.*')->where(['products.is_deleted'=>0,'products.status'=>1])->with('category','subcategory','ProductImages')
        ->leftjoin("categories as cat",function($join){
            $join->on("cat.id",'=','products.category_id');
        })
        ->leftjoin("subcategories as subcat",function($join){
            $join->on("subcat.id",'=','products.subcategory_id');
        })
        ->when($category_id,function($q,$category_id){
            return $q->where('products.category_id',$category_id);
        })
        ->when($subcategory_id,function($q,$subcategory_id){
            return $q->where('products.subcategory_id',$subcategory_id);
        })
        ->when($search,function($q,$search){
            return $q->where(function($query) use ($search){
             $query->orWhere('products.name','like','%'.$search.'%');
             $query->orWhere('products.qty','like','%'.$search.'%');
             $query->orWhere('products.code','like','%'.$search.'%');
             $query->orWhere('products.price','like','%'.$search.'%');
             $query->orWhere('products.mrp','like','%'.$search.'%');
             $query->orWhere('cat.name','like','%'.$search.'%');
             $query->orWhere('subcat.name','like','%'.$search.'%');
             $query->orWhere('products.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
         });
      })
        ->orderByDesc('products.id')->paginate($this->api_per_page);
    	if($products)
    	{
    		$data['status']  = true;
			$data['message'] = 'Products Found';
			$data['path']	 = $path;
			$data['data'] = $products;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Products not found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }




// Get all banners with playlist
    public function banners(Request $request)
    {
		$path = url('public/images/').'/';

    	$banner = Banner::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
    	if($banner)
    	{
    		$data['status']  = true;
			$data['message'] = 'Banner Found';
			$data['path']	 = $path;
			$data['data'] = $banner;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Banner Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    public function states(Request $request)
    {

        $states = DB::table('states')->get();
        if($states)
		{
			$data['status'] = true;
			$data['message'] = 'State list';
			$data['data'] = $states;
		}
        return response()->json($data);
    }
	public function cities(Request $request)
    {

        $cities = City::with('state')->get();
        if($cities)
		{
			$data['status'] = true;
			$data['message'] = 'Cities list';
			$data['data'] = $cities;
		}
        return response()->json($data);
    }
    public function order_save($order_no,$price,$user_id)
	{


		$order = new Order();
		$order->order_no = $order_no;
		$order->price = $price;
		$order->transaction_id = '';
		$order->status = 'pending';
		$order->user_id = $user_id;
		$order->order_date = date('Y-m-d');
		$order->save();

	}
    public function paytm_credential()
	{

		$data["MID"] = "oJwINa94232592477574"; // trilok test
	    // $data["MID"] = "BrIKDU59865118863813"; //trilok live
		$data["INDUSTRY_TYPE_ID"] = "Retail";
		$data["CHANNEL_ID"] = "WEB";
		// $data["WEBSITE"] = "DEFAULT"; // for live
		$data["WEBSITE"] = "WEBSTAGING"; // for test
		return $data;
	}


	public function paytm_payment_checksum(Request $request)
	{
		$data = $this->paytm_credential();

		$checkSum = "";
	// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
	$findme   = 'REFUND';
	$findmepipe = '|';
	$paramList = array();
	$paramList["MID"] = $data['MID'];
	$paramList["ORDER_ID"] = $request->ORDER_ID;
	$paramList["CUST_ID"] = $request->CUST_ID;
	$paramList["INDUSTRY_TYPE_ID"] = $data['INDUSTRY_TYPE_ID'];
	$paramList["CHANNEL_ID"] = $data['CHANNEL_ID'];
	$paramList["TXN_AMOUNT"] = $request->TXN_AMOUNT;
	$paramList["WEBSITE"] = $data['WEBSITE'];
	$paramList["CALLBACK_URL"] = $request->CALLBACK_URL;
	//$paramList["type"] = $request->type;
	//$PAYTM_MERCHANT_KEY = $data['MID'];
	//https://securegw.paytm.in/order/process //live url
	//https://securegw-stage.paytm.in/order/process // test url

	foreach($_POST as $key=>$value)
	{
	  $pos = strpos($value, $findme);
	  $pospipe = strpos($value, $findmepipe);
	  if ($pos === false || $pospipe === false)
		{
			$paramList[$key] = $value;
		}
	}

	$this->order_save($request->ORDER_ID,$request->TXN_AMOUNT,$request->CUST_ID);
	//Here checksum string will return by getChecksumFromArray() function.
	$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
	//print_r($_POST);
	 return json_encode(array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $request->ORDER_ID, "payt_STATUS" => "1","paramList"=>$paramList));
	  //Sample response return to SDK

	//  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"}
	}


	public function verify_checksum(Request $request)
	{
		$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = FALSE;

		$paramList = $_POST;
		$return_array = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum);
		//will return TRUE or FALSE string.

		// if ($isValidChecksum===TRUE)
		// 	$return_array["IS_CHECKSUM_VALID"] = "Y";
		// else
		// 	$return_array["IS_CHECKSUM_VALID"] = "N";
	    echo $isValidChecksum;
		$return_array["IS_CHECKSUM_VALID"] = $isValidChecksum ? "Y" : "N";
		//$return_array["TXNTYPE"] = "";
		//$return_array["REFUNDAMT"] = "";
		unset($return_array["CHECKSUMHASH"]);

		$encoded_json = htmlentities(json_encode($return_array));
		return json_encode($return_array);

	}
	public function get_user_email($id)
	{
		$user = User::where('id',$id)->first();
		return $user;
	}
	public function paytmCallback(Request $request)
	{
		//echo $request['data'];
		if($request['data'])
		{
		$data=  json_decode($request['data']);
		}
		else
		$data=0;

	    if('TXN_SUCCESS' == $request['STATUS'])
		{


			$order  = Order::where('order_no',$data->ORDERID)->first();

		    $order->transaction_id = $data->TXNID;
			if($data->PAYMENTMODE=="UPI")
		    $order->bankname = "UPI";
			else
		    $order->bankname = $data->BANKNAME;
		    $order->payment_mode = $data->PAYMENTMODE;
		    $order->bank_txn_id = $data->BANKTXNID;
		    $order->currency = $data->CURRENCY;
		    $order->gateway_name = $data->GATEWAYNAME;
			$order->status = 'complete';
			$order->address_id = $request->address_id ?? 0;
			$order->timeslot_id = $request->timeslot_id ?? 0;
			$order->exp_del_date = date("Y-m-d",strtotime($request->exp_del_date)) ?? date("Y-m-d");
			$order->save();



			// send notification for user
			$user = $this->get_user_email($order->user_id);
			$msg="Hii ".ucfirst($user->name)."<br>Order no: ".$order->order_id."<br>Your order is done successfully";
			$title="Order placed";
			//$this->notification($user->firebase_token, $title,$msg,'order');
			// $res = $this->send_mobile_message_url($user->contact_no,$msg);

			// For user mail
			// $this->sendOrderMail($order->order_id,$user->email,$user->name);

			// For admin mail
			$admin  = User::find('1');
			// $this->sendOrderMail($order->order_id,$admin->other_email,$admin->name);

			$msg="Hii Admin <br>Order no: ".$order->order_id."<br>".$user->name."'s order is done successfully";
		    // $res = $this->send_mobile_message_url($admin->contact_no,$msg);


			// insert User Wallet
			// if($gift_id > 0)
			// {
			// $gift_thumb = $this->user_wallet($order->user_id,$data->ORDERID,$data->TXNAMOUNT,$gift_id);
			// $data1['gift_thumb'] = $gift_thumb;
			// }
			// Redeem User Wallet
			// if($request['wallet_id'])
			// $this->redeem_wallet($request['wallet_id']);

			//For product quantity deduct
			$this->product_quantity_deduct($order->id);

			// if($cart_id > 0)
			// {
				// $this->cart_status_update($cart_id,"pending");
			// }
			$data1['status'] 				= true;
			// $data1['gift_thumb'] = $gift_thumb;
			$data1['message']	= "Order placed successfully";


		}
		else
		{
			if($data==null || $data=='')
			{
			$order  = Order::where('order_no',$request->order_id)->first();
			}
		    else
			{
			   $order  = Order::where('order_no',$data->ORDERID)->first();
			}
			$order->status = 'failed';
			$order->order_status = 'failed';
			$order->save();
			$data1['status'] = false;

			$data1['message']	= "Order failed";


		}
		return response()->json($data1,$this->successStatus);
	}
	public function product_quantity_deduct($order_id)
	{
		// For manage quantity after order success
			$order_details_count = OrderDetail::where(['order_id'=>$order_id])->where("qty",">",0)->count();
			if($order_details_count > 0)
			{
			$order_details = OrderDetail::where(['order_id'=>$order_id])->where("qty",">",0)->get();
			if($order_details)
			{
				foreach($order_details as $aa)
				{
					$product = Product::find($aa->product_id);
					$product->qty = $product->qty-$aa->qty;
					$product->save();

				}
			}
			}
	}
	public function order_details_save(Request $request)
	{

	    // $order = OrderDetail::insert($request->make_array);
		// if($request->offer_array)
	    // OrderDetail::insert($request->offer_array);
	// print_r($request->make_array);
	// exit;
		if($request->make_array)
		{

		foreach($request->make_array as $product_array)
		{
			$productDetails = Product::find($product_array['product_id']);
			$orderDetails = Order::where('order_no',$product_array['order_id'])->first();
			if($productDetails)
			{

			$orderdetail = new OrderDetail;
		    $orderdetail->user_id 			= $product_array['user_id'];
		    $orderdetail->order_id 			= $orderDetails->id ?? 0;
		    $orderdetail->order_no 			= $product_array['order_id'] ?? 0;
		    $orderdetail->order_date 		= date("Y-m-d");
		    $orderdetail->product_id 		= $product_array['product_id'];
		    $orderdetail->product_name 		= $productDetails->name;
		    $orderdetail->qty 				= $product_array['qty'];
		    $orderdetail->price     		= $product_array['price'];
		    $orderdetail->total_price     	= $product_array['total_price'];
		    $orderdetail->payment_mode     	= $product_array['payment_mode'];
		    $orderdetail->status    	 	= $product_array['status'];
		    $orderdetail->image     		= $productDetails->image;
		    $orderdetail->thumbnail     	= $productDetails->thumbnail;
		    $orderdetail->type     			= 'product';
		    $orderdetail->description     	= $productDetails->description;
		    $orderdetail->tax     			= $product_array['tax'];
			$orderdetail->save();
			}



		}
		$data['status'] 	= true;
		$data['message']	= "Order details inserted successfully";

		}
		else
		{
		$data['status'] 				= false;
		$data['message']	= "Order details not inserted successfully";
		}
		return response()->json($data,$this->successStatus);
			  //{
	// "make_array": [{
	// 		"order_id": "1",
	// 		"user_id":18,
	// 		"product_id": "1",
	// 		"qty": 1,
	// 		"price": 2500,
	// 		"total_price": 10500,
	// 		"tax":"100",
	// 		"payment_mode": "paytm",
	// 		"status": "complete"

	// 	},
	// 	{
	// 		"order_id": "1",
	// 		"user_id": 18,
	// 		"product_id": "2",
	// 		"qty": 1,
	// 		"price": 2500,
	// 		"total_price": 10500,
	// 		"tax":"100",
	// 		"payment_mode": "paytm",
	// 		"status": "complete"

	// 	}]
	// "offer_array": [{
			// "order_id": "70",
			// "user_id": 2,
			// "product_id": "2.5",
			// "product_name": "jasper",
			// "category": "",
			// "qty": "",
			// "price": 2500,
			// "total_price": "",
			// "tax":0,
			// "payment_mode": "paytm",
			// "status": "complete",
			// "description": "12",
			// "image": "2018.jpg",
			// "thumbnail": "2018.jpg"

		// },
		// {
			// "order_id": "70",
			// "user_id": 2,
			// "product_id": "2.5",
			// "product_name": "jasper",
			// "category": "",
			// "qty": "",
			// "price": 2500,
			// "total_price": "",
			// "tax":0,
			// "payment_mode": "paytm",
			// "status": "complete",
			// "description": "12",
			// "image": "2018.jpg",
			// "thumbnail": "2018.jpg"

		// }
	// ]
//}
	}
	// cod order save

	public function cod_order(Request $request)
	{
		 $validator = Validator::make($request->all(),[
			'order_id'=>'required',
			'price'=>'required',
			'address_id'=>'required',
			'timeslot_id'=>'required',

		]);



		if($validator->fails())
		{
            $data['status'] = false;
			$data['message']	= $validator->errors()->first();

             return response()->json($data);
		}
		$userinfo = Auth::user();
		$user_id = $userinfo->id;

		$user = $this->get_user_email($user_id);
		$order = new Order();
		$order->order_no = $request->order_id;
		$order->price = $request->price;
		$order->transaction_id = '';
		$order->bankname = '';
		$order->bank_txn_id = '';
		$order->currency = 'INR';
		$order->gateway_name = '';
		$order->payment_mode = 'cod';
		$order->status = 'complete';
		$order->order_status = 'pending';
		$order->type = 'product';
		$order->gift_id = 0;
		$order->user_id = $user_id;
		$order->address_id = $request->address_id ?? 0;
		$order->timeslot_id = $request->timeslot_id ?? 0;
		if($request->exp_del_date)
		$order->exp_del_date = date("Y-m-d",strtotime($request->exp_del_date));
		else
		$order->exp_del_date = date("Y-m-d");

		$order->order_date = date('Y-m-d');
		if($order->save())
		{
			//For product quantity deduct
			$this->product_quantity_deduct($order->id);
			DB::table('order_details')->where(['order_no'=> $order->order_no,'order_date'=>$order->order_date])->update(["order_id"=>$order->id]);
			$data['status'] 	= true;
			$data['message']	= "Order success";
		}
		else
		{
			$data['status'] 	= false;
			$data['message']	= "Order failed";
		}
		return response()->json($data,$this->successStatus);
	}
	public function add_address(Request $request)
	 {

		 if($request->type=='add' || $request->type=='edit')
		 {
		 $validator = Validator::make($request->all(),[
			'area'=>'required',
			'city_id'=>'required',
			'name' => 'required',
			'phone_number'=>'required',
			'pincode'=>'required',
			'state_id'=>'required',
			'type'=>'required',
			'landmark'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
		 }

			$userinfo = Auth::user();
			$user_id = $userinfo->id;
			$input = $request->all();
			$input['user_id'] = $user_id;
			if($request->type == 'add')
				{

					$user = OtherAddress::create($input);
					$type = "added";
				}
				 if($request->type == 'edit')
				{
					//echo 123;exit;
					 $validator = Validator::make($request->all(),[
						'id'=>'required',
					]);

					if($validator->fails())
					{
					 $data['status'] = false;
					 $data['message']	= $validator->errors()->first();
					  return response()->json($data);
					}
					$user = OtherAddress::find($request->id);
					$user_data = $user->fill($input)->save();

					$all_addresses = DB::table('other_addresses')->where(['is_deleted'=>0,'user_id'=>$user_id])->where('id','!=',$request->id)->update(['status'=>0]);
					$type = "updated";
				}

				 if($request->type == 'delete')
				{
					 $validator = Validator::make($request->all(),[
						'id'=>'required',
					]);

					if($validator->fails())
					{
					 $data['status'] = false;
					 $data['message']	= $validator->errors()->first();
					  return response()->json($data);
					}
				$user = DB::table('other_addresses')->where('id',$request->id)->update(['is_deleted' => 1]);
				$type = "deleted";

				}
				if($request->type=='list')
				{

					$user_count = DB::table('other_addresses')->where('user_id',$user_id)->count();
					if($user_count>0)
					{
					$user = OtherAddress::where(['is_deleted'=>0,'user_id'=>$user_id])->with('state','city')->orderBy('id','desc')->get();
					$data['data'] = $user;
				    $type = "list found";
					}
					else
					$type = "list not found";
				}
				if($user)
				{
				$data['status'] = true;
				$data['message'] = "Address $type successfully";
				return response()->json($data, $this->successStatus);
				}


	}
	public function timeslots(Request $request)
	{
		$user = Auth::user();
		$timeslots = TimeSlot::where(['is_deleted'=>0,'status'=>1])->get();
		if($timeslots)
		{
			$data['status'] 			= true;
			$data['message']			= "Time slot available";
			$data['data'] 				= $timeslots;
		}
		else
		{
			$data['status'] 			= false;
			$data['message']			= "Time slot not available";
			$data['data'] 				= '';

		}
		return response()->json($data,$this->successStatus);
	}
	public function myorders(Request $request)
	{
		$user = Auth::user();
		$orders = Order::where(['user_id'=>$user->id,'status'=>'complete'])->with('OrderDetail','OrderCancel','OrderDelivery','OrderShipping')->paginate($this->api_per_page);
		if($orders)
		{
			$data['status'] 			= true;
			$data['message']			= "Myorders list";
			$data['data'] 				= $orders;
		}
		else
		{
			$data['status'] 			= false;
			$data['message']			= "Myorders list not found";
			$data['data'] 				= '';

		}
		return response()->json($data,$this->successStatus);
	}
	public function dashboard(Request $request)
	{

		$category = Category::where(['status'=>'1','is_deleted'=>0])->with('subcategories')->get();

		$banners = Banner::where(["is_deleted"=>0,"status"=>1])->get();

		if($category || $banners)
		{
			$data['status'] = true;
			$data['message'] = 'Dashbaord details';
			$data['categories'] = $category;
			$data['banners'] = $banners;
		}
		else
		{
			$data['status'] = false;
			$data['message'] = 'Dashbaord details not found';
			$data['categories'] = '';
			$data['banners'] = '';
		}
		return response()->json($data);

	}
	public function promocodes(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if($validator->fails())
		{
            $data['status'] = false;
             if($validator->errors()->first('code'))
			 $data['message']	= $validator->errors()->first('code');

             return response()->json($data);
        }

		 $promocode = PromoCode::where(['code'=>$request->code,'is_deleted'=>0])->first();
		if($promocode)
		{

			return $this->sendResponse($promocode, 'Promo Code Applied successfully.');
		}
		else{
			 $data['status']              = false;
			 $data['message'] 			  = 'Invalid Promo Code';
			 $data['data']                = '';
			  return response()->json($data);
		}
	}














public function social_login_new(Request $request){
        try{

            $data = $request->all();

            $validator = Validator::make($request->all(),
                [
                    'social_type'  => 'required|in:GooglePlus,Facebook,Twitter',
                    'social_id' => 'required',
                    'device_type' => 'required',
                    'device_token' => 'required',
                    'email' => 'nullable',
                    'profile_picture' => 'nullable'
                ]
            );
            if($validator->fails()){
                $response['status']     = false;
                $response['message']    = $this->validationHandle($validator->messages());
                $this->response($response);
            }else{

                $check_email = false;

                $check = User::where('social_id',$data['social_id'])->exists();

                if(isset($data['email']) && $data['email'] != '')
                $check_email = User::where('email',$data['email'])->exists();

                $updateArr = [];

                if($check){

                    if(isset($data['email'])){
                        $updateArr['email'] = $data['email'];
                    }

                    if(count($updateArr) > 0)
                    User::where('social_id',$data['social_id'])->where('social_type',$data['social_type'])->update($updateArr);

                    $user = User::where('social_id',$data['social_id'])->first();


                    if($request->get('profile_picture') && !isset($user->profile_picture)){

                        $imgurl = $request->get('profile_picture');
                        $image_name = 'user_'.$user->id.'.jpeg';

                        $destinationPath = '/profile/';
                        $ppath = public_path().'/'.$destinationPath;

                        file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                        if($this->isImage('public/profile/'.$image_name)){

                            $user_img = 'public'.$destinationPath.$image_name;
                            User::where('id',$user->id)->update(['profile_picture' => $user_img]);


                        }
                    }

                    $userData = User::getProfile($user->id);
                    $jwtResponse = User::authorizeToken($user);
                    $userData->security_token = @$jwtResponse['token'];

                    $response['status']     = true;
                    $response['message']    = 'User details';
                    $response['data'] = $userData;

                    if($user){
                        UserDevices::deviceHandle([
                            "id"       =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]);
                    }

                }
                else{
                    if($check_email){

                        $user = User::where('email',$data['email'])->first();

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                $updateArr['profile_picture'] = $user_img;
                            }
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        User::where('email',$data['email'])->update($updateArr);

                        $user = User::where('email',$data['email'])->first();

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }
                    }
                    else{

                        if(isset($data['email'])){
                            $updateArr['email'] = $data['email'];
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        $user = User::Create($updateArr);

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                User::where('id',$user->id)->update(['profile_picture' => $user_img]);
                            }
                        }

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }

                    }
                }

                $this->response($response);

            }
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage().' on '.$e->getLine();
            $response['data'] = [];

            $this->response($response);
        }
    }


	 public function notification($token, $title, $description,$type)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
		$url = url('/')."/public/images/logo.jpg";
        $notification = [
            'title' => $title,
			'description'=>$description,
			'type'=>$type,
			//'image'=>$url,
            'sound' => true,
        ];

       $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'data' => $notification
        ];

        $headers = [
            'Authorization: key=AAAAtrVxdWM:APA91bFIaXDMYYdf1Qmwpj-DEqrHe98W-bJQc3hST6BhdBqq9vtXYNdzcOKk_-L24Fxd5HpaRgUkTqrEG4IMUpN-Zuab_PRG1T79Q-l7aCJCw7SU7lkrIWqnMNK4H057fqC2o91YCid6',
            'Content-Type: application/json'
        ];




        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }



    /////////////////////////////////////////////////////////////////////////////////////

}
