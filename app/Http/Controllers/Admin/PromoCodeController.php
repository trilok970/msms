<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;


use App\Models\PromoCode;
use Illuminate\Http\Request;
use Validator;
class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promocodes= PromoCode::where(['is_deleted'=>0])->orderBy("id","desc")->get();
       return view(SEGMENT.'..promocode.index',compact('promocodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(SEGMENT.'.promocode.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'code' => 'required',
            'discount' => 'required',
            'type' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        
        $promocode = new PromoCode();
        $promocode->code = $request->code ?? 0;
        $promocode->type = $request->type ?? 'flat';
        $promocode->discount = $request->discount ?? 0;
        if($promocode->save())
        {
            return redirect(SEGMENT.'/promocode')->with('message','Prome code added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function show(PromoCode $promocode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function edit(PromoCode $promocode)
    {
        return view(SEGMENT.'.promocode.edit',compact('promocode'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromoCode $promocode)
    {
       $validator = Validator::make($request->all(),[
            'code' => 'required',
            'discount' => 'required',
            'type' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        
        $promocode->code = $request->code ?? 0;
        $promocode->type = $request->type ?? 'flat';
        $promocode->discount = $request->discount ?? 0;
        if($promocode->save())
        {
            return redirect(SEGMENT.'/promocode')->with('message','Prome code updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromoCode $promocode)
    {
        $promocode->is_deleted = 1;
        if($promocode->save())
        {
            return redirect(SEGMENT.'/promocode')->with('message','Prome code deleted successfully');
        }
    }
}
