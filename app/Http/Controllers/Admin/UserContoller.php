<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\PromoCode;
use App\Models\Banner;
use App\Models\Product;
use Validator;
use DB;
use Session;
use Hash;
use Illuminate\Support\Facades\Password;
class UserContoller extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset_password(Request $request) 
    {
        
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed|min:8|max:20'
        ]);
        
// echo "<pre>";
//         dd(request());
//         exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();

            User::where(['email'=>$user->email])->update(['password'=>$user->password]);
        });
        $status=1;
        if ($reset_password_status == Password::INVALID_TOKEN) {
            $status=0;
            // return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
            $message = "Invalid token provided.";
            return redirect('success')->with('error_message',$message);
        }

        // return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
        $message = "Password has been changed successfully.";
        return redirect('success')->with('message',$message);
    }
    public function success_message()
    {
        return view('auth.success');
    }
    public function login()
    {
        return view('auth.login');
    }
    public function do_login(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);exit;
        $validator = Validator::make($request->all(),[
            'email'=>'required',
            'password'=>'required',
        ]);
        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }
        $count = Admin::where(['email'=>$request->email])->count();
        
        if($count > 0)
        {
            if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password,'id'=>1]))
            {
                return redirect(SEGMENT.'/dashboard');
            }
            else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
        }
        else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(SEGMENT);
    }
    public function index(Request $request)
    {
        //
        if($request->query())
        {
            $search = trim($request->search);
            $users = User::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere('fullname','like','%'.$search.'%');
                $query->orWhere('phone_number','like','%'.$search.'%');
                $query->orWhere('email','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id')->paginate($this->paginate_no);
        }
        else
        {
            $users = User::where(['is_deleted'=>0])->where('id','!=',1)->orderByDesc('id')->paginate($this->paginate_no);
            $search = '';
        }
       
        return view(SEGMENT.'/user.index',compact('users','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
       $user->is_deleted = 1;
       if($user->save())
       {
        // $token =  DB::table('oauth_access_tokens')
        //     ->where('user_id', $user->id)
        //     ->update(['revoked'=>1]);
        return back()->with('message','User Deleted Successfully');
       }
       else
        return back()->with('message','Something Went Wrong');

    }

    public function update_status(Request $request)
    {
        
        if($request->type=="channel")
        {
        $banner = Channel::where('id',$request->id)->first();
        $banner->status = $request->status;
        // $this->send_update_notification("banner");
        }
        else if($request->type=="category")
        {
            $banner = Category::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("category");
        }
        else if($request->type=="subcategory")
        {
            $banner = Subcategory::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("subcategory");
        }
        else if($request->type=="product")
        {
            $banner = Product::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("innercategory");
        }
        else if($request->type=="banner")
        {
            $banner = Banner::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="promocode")
        {
            $banner = PromoCode::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        elseif($request->type=="users")
        {
        $banner = User::where('id',$request->id)->first();
        $banner->status = $request->status; 

        if($request->status==0)
        {
            // $token =  DB::table('oauth_access_tokens')
            // ->where('user_id', $banner->id)
            // ->update(['revoked'=>1]);
            // print_r($token);
        }
        
        
        }
        if($banner->save())
        {
             
             // session()->put('success','Status change successfully');
            $data['status'] = 1;
            return response()->json($data);
        }
        else{
            session()->put('warning','Status not change successfully');
            $data['status'] = 0;
            return response()->json($data);
        }
    }


public function ajax(Request $request)
     {

        $data = '';
     if($request->type=='innercategory')
     {
         
        $subcategories = Innercategory::select('id','name')->where(['sub_category_id'=>$request->subcategory_id,'category_master_id'=>$request->category_master_id,'is_deleted'=>0])->get();
        $data .= '<option value="0">-Select-</option>';
     }
    
     else if($request->type=='subcategory')
     {
        $subcategories = Subcategory::select('id','name')->where(['category_id'=>$request->category_id,'is_deleted'=>0])->get();
        $data .= '<option value="0">-Select-</option>';
     }
       
       // print_r($subcategories);
      
      // exit;
      foreach($subcategories as $subcategorie)
      { 
        $data .= '<option name="'.ucfirst($subcategorie->name).'" value="'.$subcategorie->id.'">'.$subcategorie->name.'</option>';
     
      }
     return ($data);

      

    }



    public function profile()
    {
        return view('admin.commons.coming-soon');
    }
     public function animatedBanner()
    {
        return view('admin.commons.animated-banner');
    }
     public function ambianceAudio()
    {
        return view('admin.commons.ambiance-audio');
    }






    ////////////////////////////////////////////////////////////////////////////////////////////////
}
