<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use DB;
use ImageResize;
use Validator;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $products = Product::where(['is_deleted'=>0])->orderByDesc('id')->get();

        return view(SEGMENT.'.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        $units = DB::table('units')->orderByDesc('id')->get();

        return view(SEGMENT.'.product.create',compact('categories','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'qty' => 'required',
            'min_qty' => 'required',
            'max_qty' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'description' => 'required',
            'image' => 'required',
            'thumbnail' => 'required',
            'unit' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $images=array();

    if($request->hasFile('image'))
      {
        if($files=$request->file('image')){
                foreach($files as $file){
                    $name = "uploads/images/".time().$file->getClientOriginalName();
                    $ff = $file->move(public_path('/uploads/images'),$name);

                    $thumbnail = time().$file->getClientOriginalName();
                    $destinationPath = public_path('/uploads/images');
                    $img = ImageResize::make($ff->getRealPath());
                    $img->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);

                    $images[]=$name;
                }
            }
            
              $fileName = implode("|",$images);

        // $extension = $request->image->extension();
        // $fileName  = "uploads/images/".time().".$extension";
        // $ff = $request->image->move(public_path('uploads/images'),$fileName);
        
        // $thumbnail = time().".$extension";;
        //         $destinationPath = public_path('/uploads/images');
        //         $img = ImageResize::make($ff->getRealPath());
        //         $img->resize(500, 500, function ($constraint) {
        //         $constraint->aspectRatio();
        //         })->save($destinationPath.'/'.$thumbnail);
      }
      else
      {
            $fileName = "default.jpg";
      }

      if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
      {
        $extension1 = $request->thumbnail->extension();
        $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
        $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);
       
       $thumbnail1 = time().".$extension1";;
                $destinationPath1 = public_path('/uploads/thumbnail');
                $img1 = ImageResize::make($ff1->getRealPath());
                $img1->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath1.'/'.$thumbnail1);
      }
      else
      {
            $fileNameThumb = "default.jpg";
      }
      
       // echo $fileName;exit;
        $product = new Product();
        $product->name = ucwords($request->name);
        $product->category_id = $request->category_id ?? 0;
        $product->subcategory_id = $request->subcategory_id ?? 0;
        $product->code = $request->code ?? 0;
        $product->description = $request->description ?? "";
        $product->qty = $request->qty ?? 0;
        $product->min_qty = $request->min_qty ?? 0;
        $product->max_qty = $request->max_qty ?? 0;
        $product->mrp = $request->mrp ? $request->mrp:0;
        $product->price = $request->price ? $request->price:0;
        $product->image = $fileName;
        $product->thumbnail = $fileNameThumb;
        $product->weight = $request->weight ?? 0;
        $product->unit = $request->unit ?? 0;
        if($product->save())
        {

            if(count($images) > 0)
            {
                foreach ($images as $key => $value) {
                    $productimages = new ProductImage;
                    $productimages->product_id = $product->id;
                    $productimages->image = $value;
                    $productimages->save();
                }
                
            }
        // $type = "Category Added Suceesfully";
        // $msg = "your";
        // $this->get_notification($type, $msg);
            return redirect(SEGMENT.'/product')->with('message','Product added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        $subcategories = Subcategory::where(['is_deleted'=>0,'category_id'=>$product->category_id])->orderByDesc('id')->get();
        $units = DB::table('units')->orderByDesc('id')->get();

        return view(SEGMENT.'.product.edit',compact('categories','units','product','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'qty' => 'required',
            'min_qty' => 'required',
            'max_qty' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'description' => 'required',
            'unit' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $images=array();

    if($request->hasFile('image'))
      {
        if($files=$request->file('image')){
                foreach($files as $file){
                    $name = "uploads/images/".time().$file->getClientOriginalName();
                    $ff = $file->move(public_path('/uploads/images'),$name);

                    $thumbnail = time().$file->getClientOriginalName();
                    $destinationPath = public_path('/uploads/images');
                    $img = ImageResize::make($ff->getRealPath());
                    $img->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);

                    $images[]=$name;
                }
            }
            
              $fileName = implode("|",$images);

        // $extension = $request->image->extension();
        // $fileName  = "uploads/images/".time().".$extension";
        // $ff = $request->image->move(public_path('uploads/images'),$fileName);
        
        // $thumbnail = time().".$extension";;
        //         $destinationPath = public_path('/uploads/images');
        //         $img = ImageResize::make($ff->getRealPath());
        //         $img->resize(500, 500, function ($constraint) {
        //         $constraint->aspectRatio();
        //         })->save($destinationPath.'/'.$thumbnail);
      }
      else
      {
            $fileName = $product->image;
      }

      if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
      {
        $extension1 = $request->thumbnail->extension();
        $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
        $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);
       
       $thumbnail1 = time().".$extension1";;
                $destinationPath1 = public_path('/uploads/thumbnail');
                $img1 = ImageResize::make($ff1->getRealPath());
                $img1->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath1.'/'.$thumbnail1);
      }
      else
      {
            $fileNameThumb = $product->thumbnail;
      }
      
       // echo $fileName;exit;
        
        $product->name = ucwords($request->name);
        $product->category_id = $request->category_id ?? 0;
        $product->subcategory_id = $request->subcategory_id ?? 0;
        $product->code = $request->code ?? 0;
        $product->description = $request->description ?? "";
        $product->qty = $request->qty ?? 0;
        $product->min_qty = $request->min_qty ?? 0;
        $product->max_qty = $request->max_qty ?? 0;
        $product->mrp = $request->mrp ? $request->mrp:0;
        $product->price = $request->price ? $request->price:0;
        $product->image = $fileName;
        $product->thumbnail = $fileNameThumb;
        $product->weight = $request->weight ?? 0;
        $product->unit = $request->unit ?? 0;
        if($product->save())
        {

            if(count($images) > 0)
            {
                foreach ($images as $key => $value) {
                    $productimages = new ProductImage;
                    $productimages->product_id = $product->id;
                    $productimages->image = $value;
                    $productimages->save();
                }
                
            }
        // $type = "Category Added Suceesfully";
        // $msg = "your";
        // $this->get_notification($type, $msg);
            return redirect(SEGMENT.'/product')->with('message','Product updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
