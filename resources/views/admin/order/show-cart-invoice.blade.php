<?php
$current_month = date("M");
$current_day = date("d");
$current_year = date("Y");
$full_name = "Trilok";//session('full_name');

		$address = $user->address." ".$user->city." ".$user->state;
		

	
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" style="background-color:#CCCCCC; padding-top:20px; padding-bottom:20px;"><table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td style="background-color:#FFFFFF;"><table width="670" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="169" height="20">&nbsp;</td>
            <td width="501">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="{{url('public/images/image1554370531.png')}}" target="_blank"><img src="{{url('public/images/image1554370531.png')}}" alt="" border="0" height="80" /></a></td>
            <td align="right" style="font-family:Arial, Helvetica, sans-serif;"><strong>Notification</strong><br /><?php echo $current_month.' '.$current_day.', '.$current_year;?></td>
          </tr>
          <tr>
            <td style="height:20px; border-bottom:1px solid #CCCCCC;">&nbsp;</td>
            <td style="border-bottom:1px solid #CCCCCC;">&nbsp;</td>
          </tr>
          <tr>
            <td style="height:20px;">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
          <table width="670" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td style="font-family:Arial, Helvetica, sans-serif;"><p>Hi  <?php echo ucwords($user->name); ?> </p>
<!-----------------------------invoice start ------------------------------------------>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
		  <tr>
			<td  height="30" style="border-bottom:1px solid #CCCCCC;">Order Number: <strong>{{$order_id}}</strong> </td>
			<td  height="30" style="border-bottom:1px solid #CCCCCC;">Order Date: <strong>{{date("d-M-Y h:i A",strtotime($main_order->created_at))}}</strong> </td>
			<td  align="right" style="border-bottom:1px solid #CCCCCC;">Order Status: <b>{{ucfirst($main_order->order_status)}}</b> </td>
		  </tr>
		</table>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
		  <tr>
			<td width="335" valign="top" style="border-right:1px solid #CCCCCC; padding-top:12px; padding-bottom:12px;"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
        <td width="25%" valign="top"><strong>Bill To: </strong></td>
        <td width="75%" valign="top"><strong>{{ucwords($user->firm_name)}}</strong>
		  </td>
      </tr>
			  <tr>
        <td width="25%" valign="top">&nbsp;</td>
        <td width="75%" valign="top">{{ucwords($address)}} <br> {{ $user->contact_no}}
		  </td>
      </tr>
	  
    </table></td>
    <td width="335" valign="top" style="padding-top:12px; padding-bottom:12px;"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
        <td width="25%" valign="top"><strong>Ship To: </strong></td>
        <td width="75%" valign="top"><strong>{{ucwords($user->firm_name)}}</strong>
		  </td>
      </tr>
      <tr>
        <td width="25%" valign="top">&nbsp;</td>
        <td width="75%" valign="top">{{ucwords($address)}} <br> {{ $user->contact_no}}</td>
      </tr>
    </table></td>
  </tr>
</table>

<table width="100%" border="0" align="center" cellpadding="6" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
  <tr>
    <td width="375" align="left" style="background-color:#000000; color:#FFFFFF;"><strong>Product Description</strong></td>
    <td width="80" align="center" style="background-color:#000000; color:#FFFFFF;"><strong>Qty.</strong></td>
    <td width="111" align="right" style="background-color:#000000; color:#FFFFFF;"><strong>Unit Price (INR) </strong></td>
	<td width="80" align="right" style="background-color:#000000; color:#FFFFFF;"><strong>Tax (INR) </strong></td>
    <td width="150" align="right" style="background-color:#000000; color:#FFFFFF;"><strong>Total Price (INR) </strong></td>
  </tr>

@php $t_price = 0; @endphp
@foreach($order_details as $order)
	
@if($order->type=="product")
@php	$product_qty="(Qty - ".$order->gift_qty.")"; @endphp
@else
@php	$product_qty=""; @endphp
@endif
<tr>
    <td align="left" style="border-bottom:1px solid #CCCCCC;">
	{{$order->product_name}} <b>{{$product_qty}}</b></td>
    <td align="center" style="border-bottom:1px solid #CCCCCC;">
	@if($order->qty==0)
	@php
		$type=ucfirst($order->type); @endphp
		@if($type=="Todayoffer")
	@php	$type="Today's Offer";	@endphp
		@endif
	
	@else
	    @php $type=$order->qty; @endphp
	@endif
		{{$type}}</td>
    <td align="right" style="border-bottom:1px solid #CCCCCC;">
	@if($order->type=='')
		@php $price=$order->price; 
	 $t_price +=$order->price*$order->qty;@endphp
	@else
		@php $price=''; @endphp
	@endif
	
	{{number_format($price,2)}}</td>
	<td align="right" style="border-bottom:1px solid #CCCCCC;">{{number_format($order->tax,2)}}</td>
    <td align="right" style="border-bottom:1px solid #CCCCCC;">{{number_format($order->total_price,2)}}</td>
  </tr>
	
@endforeach 



<tr>
    <td align="left" >&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="right" colspan="2">Total</td>
    <td align="right">{{number_format(($t_price),2)}}</td>
  </tr>
 
  <tr>
    
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="right" colspan="2">Total Tax </td>
    <td align="right">{{number_format($total_tax,2)}}</td>
  </tr>
  
  <tr>
    
    <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
    <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
    <td align="right" bgcolor="#CCCCCC" colspan="2"><strong>Grand Total</strong></td>
    <td align="right" bgcolor="#CCCCCC"><strong>{{number_format(round($t_price+$total_tax),2)}}</strong></td>
  </tr>
  <tr>
  <td align="left"  colspan="5" style="border-bottom:1px solid #CCCCCC;"><b>Offer Applied</b></td>
  </tr>
  @foreach($offer_details as $order)
	
@if($order->type=="product")
@php	$product_qty="(Qty - ".$order->gift_qty.")"; @endphp
@else
@php	$product_qty=""; @endphp
@endif
<tr>
    
    <td align="left" style="border-bottom:1px solid #CCCCCC;">
	
	@php $type=$order->type; @endphp
	@if($type=="Todayoffer")
	@php	$type="Today's Offer";	@endphp
	@elseif($type=="product")
	@php $type = $order->product_name;	@endphp
	@else
	@php $type = ucfirst($order->type); @endphp
	@endif
	{{$type}}</td>
	<td align="left" colspan="4" style="border-bottom:1px solid #CCCCCC;">
    @if($order->type=="product")
	@php $product_name ="";	@endphp
	@else
	@php $product_name =ucfirst($order->product_name);	@endphp	
	@endif	
	{{$product_name}} <b>{{$product_qty}}</b></td>
   
  </tr>
	
@endforeach 
 <tr>
    <td align="left" colspan="5" style="border-bottom:1px solid #CCCCCC;"><b>Advance Payment Offer   </b><br>Free Goods on Mrp value is equal to 10 % of invoice value <b>{{number_format((($main_order->price - $total_tax)*(.10)),2)}}</b></td>
     
  </tr> 
   <tr>
     <td align="left" colspan="5" style="border-bottom:1px solid #CCCCCC;"><b>Freight Paid</b><br>Free delivery above Rs. 500+GST(*Condition apply)</td>
  </tr> 
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
  <tr>
    <td width="335" style="padding:12px; border-bottom:1px solid #CCCCCC;">
	<p style="font-weight:bold;">Frequently Asked Questions</p>
	<p style="font-weight:bold;">When will I get my items?</p>
	<p>You will receive an email in 1 - 8 hrs. with delivery time and tracking details once your order is confirmed.</p>
	</td>
    <td width="335" style="padding:12px; border-left:2px solid #CCCCCC; border-bottom:1px solid #CCCCCC;">
	<p style="font-weight:bold;">How do I get in touch with seller?</p>
	<p>You can get all the contact details of the seller mentioned at top on your invoice. If you still have any further queries you can raise your issue by sending us an email on <a href="mailto:info@jasper4u.com?subject= Ref: Order Number # '.$order_id.'">info@jasper4u.com</a>. Please ensure that you have the order number for better assistance.</p>
	</td>
  </tr>
</table>
			
			
			
			
			</td>
            </tr>
            <tr>
              <td style="font-family:Arial, Helvetica, sans-serif;"><br />
			  Thank you<br />
			  Jasper4U Chemist ka sathi <br/>
			  <a href="http://jasper4u.com/">http://jasper4u.com/</a>
			  <br/>
			  <br/>
			  </td>
            </tr>
            <tr>
              <td style="border-top:1px solid #CCCCCC; padding-top:15px;"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="20">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
    </table>
      <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align:center; padding-top:15px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
            
            
          </td>
        </tr>
      </table></td>
  </tr>
</table>