 <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/user')}}" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">Users</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/banner')}}" aria-expanded="false"><i class="mdi mdi-folder-multiple-image"></i><span class="hide-menu">Banner</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/category')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Category</span></a></li>

 
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/subcategory')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Subcategory</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/product')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Product</span></a></li>

                         <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/promocode')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Promo code</span></a></li>
                        


                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/cms')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> CMS Pages </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/content')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Content </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/animated-banner')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Animated Banner </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/ambiance-audio')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Ambiance Audio </span></a></li>
                                
                            </ul>
                        </li>

                       
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">