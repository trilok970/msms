@extends('layouts.admin')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Contents</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('content')}}">Contents</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">
                               
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">All Contents</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url('content')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Content" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Content
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                   <table class="table table-striped table-bordered">
                               <tbody>
                                   
                                    <tr>
                                        <th style="width:30%">Title</th>
                                        <td style="width:70%">{{$content->title}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Sub Title</th>
                                        <td style="width:70%">{{$content->sub_title}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Description</th>
                                        <td style="width:70%">{!! $content->description !!}</td>
                                    </tr>
                                                                        <tr>
                                        <th style="width:30%">Image</th>
                                        <td style="width:70%">
                                            <div class="avatar-xxl">
                                                <img src="{{url('images/'.$content->image)}}" class="avatar-img"> 
                                            </div>
                                        </td>
                                    </tr>
                                                                        <tr>
                                        <th>Status</th>
                                        <td>Active</td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>

                            </div>
                        </div>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
          
@endsection