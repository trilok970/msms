@extends('layouts.admin')
@section('title','Playlist Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    
                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif
                    
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url(SEGMENT.'/playlist/'.$playlist->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method('PUT')
                   <div class="card">
                    
                    
                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/playlist')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Playlist" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlist
                                        </a>
                                    </div>
                            </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Channel</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="channel_id" name="channel_id" placeholder="" value="{{old('channel_id')}}">
                                        <option value="">Select Channel</option>
                                        @foreach($channels as $channel)
                                        <option value="{{$channel->id}}" {{$playlist->channel_id==$channel->id ? "selected":""}}>{{$channel->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('channel_id')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="category_id" name="category_id" placeholder="" value="{{old('category_id')}}" onchange="show_subcategory()">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}" {{$playlist->category_id==$cat->id ? "selected":""}}>{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('category_id')}}</span>
                                    </div>
                                </div>
                               <!--  <div class="form-group row">
                                    <label class="col-md-2 m-t-15">SubCategory</label>
                                    <div class="col-md-10">
                                        <select type="text" class="form-control" id="subcategory_id" name="subcategory_id" placeholder="" value="{{old('subcategory_id')}}">
                                        <option value="">Select SubCategory</option>
                                       @foreach($subcategories as $sub)
                                        <option value="{{$sub->id}}" {{$playlist->subcategory_id==$sub->id ? "selected":""}}>{{$sub->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('subcategory_id')}}</span>
                                    </div>
                                </div>-->
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Title" value="{{$playlist->name}}">
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                              <!-- <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-10">
                                       <textarea class="form-control" id="description" name="description" placeholder="Enter Description" value="{{$playlist->description}}" >{{$playlist->description}}</textarea> 
                                        <span class="text-danger">{{$errors->first('description')}}
                                    </div>
                                </div>
                               
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="image" name="image" placeholder="Choose image" value="{{old('image')}}">

                                    </div>
                                    <div class="col-lg-4">
                                    <img  id="image_preview"  src="{{url('images/'.$playlist->image)}}" width="140" class="pull-right" alt="User Image">
                                    </div>
                                </div> -->
                                @if(count($playlist->songs) > 0)
                                <div class="form-group row">
                                    <label class="col-md-12 m-t-15">Audio Files List</label>
                                    
                                        
                                            
                                    @foreach($playlist->songs as $file)
                                   
                                    <label class="col-md-2 m-t-15 div_{{$file->id}}"></label>
                                    <label class="col-md-2 m-t-15 div_{{$file->id}}">{{$file->title}}</label>
                                    <div class="col-md-3 div_{{$file->id}}"><a target="_blank" href="{{url('uploads/images/'.$file->image)}}"><img src="{{url('uploads/images/'.$file->image)}}" width="50" /></a></div>

                                    <div class="col-md-5 div_{{$file->id}}" >
                                        <button style="float: left;" type="button" class="close" aria-label="Close">
                                              <span class="close" id="{{$file->id}}" aria-hidden="true" style="margin-left: 86%;" data-toggle="tooltip" title="Delete Audio">&times;</span>
                                              

                                                <audio controls>
                                                <source src="{{ url('uploads/audio/'.$file->file) }}" type="audio/{{$file->extension}}">
                                                Your browser does not support the audio element.
                                                </audio>
                                               
                                            </button>
                                            </div>  

                                    
                                    
                                            @endforeach
                               
                                          
                                            
                                    
                                   
                                </div>
                                @endif 

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><b><i class="fa fa-plus-circle"></i> Add Audio Files</b></label>
                                     
                               </div> 
                              

                                <input type="hidden" name="total_no" id="total_no" value="0">
                                <div id="add_audio"></div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-1">
                                    <button type="button" class="btn btn-success" id="add"><i class="fa fa-plus-circle"></i> Add</button>
                                       

                                    </div>
                                    <div class="col-lg-1">
                                    <button style="display: none;" type="button" class="btn btn-danger" id="remove"><i class="fa fa-minus-circle"></i> Remove</button>
                                    
                                    </div>
                                </div>

                                
                                
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/playlist')}}" class="btn btn-danger resetBtn">Cancel</a>
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
          
          <script>
    function show_subcategory()
    {
    var category_id = $('#category_id').children('option:selected').attr('value');
    console.log("category_id >> "+category_id);
    
    $.ajax({
        
        
        type:'get',
        url:"{{url(SEGMENT.'/ajax')}}",
        data:{category_id:category_id,'type':'subcategory'},
        success:function(data){
    
                $('#subcategory_id').html(data);
                }
            });
    
    }
    function remove_button()
    {
      var total_no = parseInt($("#total_no").val());
      if(total_no > 0)
            $("#remove").show();
        else
            $("#remove").hide();

    }
        $(document).ready(function(){
           
            $("#add").click(function(){
            var total_no = parseInt($("#total_no").val());
            console.log('total_no >> '+total_no);
                total_no = total_no + 1;
            var add_audio = '<div class="form-group"><div class="form-group row"><label class="col-md-2 m-t-15"><b>Sr. No.</b></label><div class="col-md-10">'+total_no+'</div></div><div class="form-group row"><label class="col-md-2 m-t-15">Title</label><div class="col-md-10"><input type="text" class="form-control" id="title" name="title[]" placeholder="Enter Title" value=""></div></div> <div class="form-group row"><label class="col-md-2 m-t-15">Image</label><div class="col-md-6"><input type="file" class="form-control image" inc_val="'+total_no+'"  name="image[]" placeholder="Choose image" value=""></div><div class="col-lg-4"><img style="display:none;"  id="image_preview'+total_no+'"  src="" width="50" class="pull-right" alt="User Image"></div></div>  <div class="form-group row"><label class="col-md-2 m-t-15">Audio Files</label><div class="col-md-10"><input type="file" class="form-control"  name="file[]" placeholder="Choose audio"></div></div></div> ';
            $("#add_audio").append(add_audio);
            $("#total_no").val(total_no);
            $(".image").change(function() {
                var inc_val = $(this).attr('inc_val');
            readURL(this,inc_val);
            });
            remove_button();
        });

        $("#remove").click(function(){
            var total_no = parseInt($("#total_no").val());
            console.log('total_no >> '+total_no);
                total_no = total_no - 1;
            $("#add_audio").children().last().remove();
            if(total_no >= 0)
            $("#total_no").val(total_no);
            remove_button();

        });
        
        $(".close").click(function(){
            var id = $(this).attr('id');
            var img_url = $("#img_url").val();
            
            console.log("id >> "+id);
            if(id)
            {
                bootbox.confirm({
                message: "Are you sure you want to delete this audio ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {

                    
                if(result)
                {
                    $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                        url:"{{url(SEGMENT.'/songs-delete')}}",
                        type:"post",
                        beforeSend:function(){
                            $("#loadingmessage").show();
                        },
                        data:{id:id},
                        success:function(data){
                            $("#loadingmessage").hide();
                            if(data.status==1)
                            {
                                $(".div_"+id).hide();
                                var msg= "Audio Deleted Successfully";
                                var title= "Success";
                                var type= "success";
                                notification_msg(msg,title,type);
                            }
                            else
                            {
                                var msg= "Audio Not Deleted";
                                var title= "Error";
                                var type= "error";
                                notification_msg(msg,title,type);
                            }
                      

                        }
                    }); // Ajax close
                }
                else
                {
                    var msg= "Audio Not Deleted";
                    var title= "Error";
                    var type= "error";
                    notification_msg(msg,title,type);
                }
                    
                }
            });
            }

        })

        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                channel_id: {
                    required: true
                },
                category_id: {
                    required: true
                },
                name: {
                    required: true
                },
                
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });
         
    
     function readURL(input) 
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });
        
        
        
        });

        

        
        
        
    </script>
@endsection