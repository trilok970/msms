<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'App\Http\Controllers\API'],function(){

Route::post('register', 'PassportController@register');
Route::post('login', 'PassportController@login');
Route::post('forgot-password', 'PassportController@forgot_password');
Route::post('social-check', 'PassportController@social_check');
Route::post('password/reset', 'PassportController@reset');
Route::get('test-mail', 'PassportController@test_mail');




Route::group(['middleware' => ['auth:api','user_accessible']], function(){
Route::post('details', 'PassportController@details');

Route::post('edit-profile', 'PassportController@edit_profile');
Route::get('get-profile', 'PassportController@get_profile');
Route::post('change-password', 'PassportController@change_password');
Route::post('logout', 'PassportController@logout');
Route::get('categories', 'PassportController@categories');
Route::post('subcategories', 'PassportController@subcategories');
Route::post('products', 'PassportController@products');
Route::get('banners', 'PassportController@banners');
Route::post('add-address', 'PassportController@add_address');
Route::post('paytm-payment-checksum', 'PassportController@paytm_payment_checksum');
Route::post('order-details-save','PassportController@order_details_save');
Route::get('timeslots','PassportController@timeslots');
Route::get('states','PassportController@states');
Route::get('cities','PassportController@cities');
Route::post('cod-order','PassportController@cod_order');
Route::post('paytm-callback','PassportController@paytmCallback');
Route::get('myorders','PassportController@myorders');
Route::get('dashboard','PassportController@dashboard');
Route::post('promocodes','PassportController@promocodes');




});

});
