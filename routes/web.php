<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
define('SEGMENT',request()->segment(1));

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('success','UserContoller@success_message');




Route::group(['prefix'=>'admin','namespace'=>'App\Http\Controllers\Admin'],function(){

	Route::get('/', 'UserContoller@login');
	Route::get('login', 'UserContoller@login');
	Route::post('login', [ 'as' => 'login', 'uses' => 'UserContoller@login']);
	Route::post('do-login','UserContoller@do_login');
	Route::get('logout','UserContoller@logout');
	Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
	Route::post('reset-password','UserContoller@reset_password');;

	Route::middleware(['admin'])->group(function () {

		Route::get('dashboard','DashboardController@index');
		Route::resource('user','UserContoller');
		Route::post('update-status','UserContoller@update_status');
		Route::get('ajax','UserContoller@ajax');
		Route::resource('cms','CmsController');
		Route::resource('content','ContentController');
		Route::resource('category','CategoryController');
		Route::resource('subcategory','SubCategoryController');
		Route::resource('product','ProductController');
		Route::resource('banner','BannerController');
		Route::resource('promocode','PromoCodeController');
		Route::post('banner-add/{id}','PlaylistController@banner_add_post');
		Route::get('profile','UserContoller@profile')->name('profile');
        Route::get('orders/{id?}','OrderController@orders');
        Route::resource('order','OrderController');
        Route::get('/update-order-status/{order_id}/{user_id}/{order_Status}','OrderController@update_order_status');
        Route::post('/update-order-status','OrderController@update_order_status1');


	});


});
