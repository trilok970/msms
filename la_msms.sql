-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2020 at 02:54 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `la_msms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Eugene Weimann MD', 'admin@gmail.com', '2020-11-22 20:12:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7V4xJAfTbq', '2020-11-22 20:12:07', '2020-11-22 20:12:07');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `thumbnail`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Shyam baba dress', 'images/image1606097260.jpeg', 'default.jpg', 1, 0, '2020-11-22 20:37:40', '2020-11-23 20:17:30'),
(2, 'New collection', 'images/1606267022.jpeg', 'default.jpg', 1, 0, '2020-11-23 20:18:14', '2020-11-24 19:47:02'),
(3, 'New', 'uploads/images/1606268239.jpeg', 'default.jpg', 1, 0, '2020-11-24 20:07:19', '2020-11-24 20:07:19'),
(4, 'Aa', 'uploads/images/1606268645.jpeg', 'default.jpg', 1, 0, '2020-11-24 20:14:05', '2020-11-24 20:14:05'),
(5, 'Aa', 'uploads/images/1606268783.jpeg', 'uploads/thumbnail/1606268783.jpeg', 1, 0, '2020-11-24 20:16:23', '2020-11-24 20:16:23');

-- --------------------------------------------------------

--
-- Table structure for table `device_tokens`
--

CREATE TABLE `device_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `device_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_tokens`
--

INSERT INTO `device_tokens` (`id`, `user_id`, `device_type`, `device_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Android', 'sdhfjashdfjshdfjh', '2020-11-24 19:15:03', '2020-11-24 19:15:03'),
(2, 0, 'Android', 'sdhfjashdfjshdfjh', '2020-11-24 19:16:35', '2020-11-24 19:16:35'),
(3, 17, 'Android', 'sdhfjashdfjshdfjh', '2020-11-24 19:17:32', '2020-11-24 19:17:32'),
(4, 18, 'Android', 'sdhfjashdfjshdfjh', '2020-11-24 19:18:14', '2020-11-24 19:18:14');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_23_011256_create_admins_table', 1),
(5, '2020_11_23_015940_create_categories_table', 2),
(6, '2020_11_23_023022_create_device_tokens_table', 3),
(7, '2020_11_24_015016_create_subcategories_table', 4),
(8, '2016_06_01_000001_create_oauth_auth_codes_table', 5),
(9, '2016_06_01_000002_create_oauth_access_tokens_table', 5),
(10, '2016_06_01_000003_create_oauth_refresh_tokens_table', 5),
(11, '2016_06_01_000004_create_oauth_clients_table', 5),
(12, '2016_06_01_000005_create_oauth_personal_access_clients_table', 5),
(13, '2020_11_24_020720_create_products_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('009cce968cad472a82f4499fc5f546fbc18f12dde1ef9e7ce28401e34972b5c8a162cf2018ae2ebd', 17, 1, 'MyApp', '[]', 0, '2020-11-24 19:17:32', '2020-11-24 19:17:32', '2021-11-25 00:47:32'),
('447d006a062a549cb383143ca67165a6303e7ed71246256d99d064d722ecacc8aec30325d75aac55', 15, 1, 'MyApp', '[]', 0, '2020-11-24 19:15:04', '2020-11-24 19:15:04', '2021-11-25 00:45:04'),
('5127af8ac9cbf169da507722ce0ec02d34bc6d46b4ac1b613827ec8126249e4ddc53d5b9269a3720', 18, 1, 'Laravel Password Grant Client', '[]', 0, '2020-11-24 19:21:43', '2020-11-24 19:21:43', '2021-11-25 00:51:43'),
('a3fd05b0f05072966d530865fc9088bcfe43a4a7f74c00a88ede22deab8a711caaf2cc282bbb81ba', 18, 1, 'MyApp', '[]', 0, '2020-11-24 19:18:14', '2020-11-24 19:18:14', '2021-11-25 00:48:14'),
('d5705a18397b8f5cce9b12ba6a14529a861de572c29c8ea31334dfa51c5b454b0560ddc9049209cc', 16, 1, 'MyApp', '[]', 0, '2020-11-24 19:16:35', '2020-11-24 19:16:35', '2021-11-25 00:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Mera Sarveshvar Mera Shyam Personal Access Client', '55GzbiwlkBCH9ixS9aUUnWWXiVoqVeVAcGwPxy2S', NULL, 'http://localhost', 1, 0, 0, '2020-11-23 20:42:52', '2020-11-23 20:42:52'),
(2, NULL, 'Mera Sarveshvar Mera Shyam Password Grant Client', 'YAV51gtwOfVrL12sDsMvrMIWOC56T8me8iBGIWh1', 'users', 'http://localhost', 0, 1, 0, '2020-11-23 20:42:52', '2020-11-23 20:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-23 20:42:52', '2020-11-23 20:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `status` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `image`, `thumbnail`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 2, 'New Dress', 'images/1606266946.jpeg', 'default.jpg', 1, 0, '2020-11-23 20:28:50', '2020-11-24 19:45:46'),
(2, 2, 'New', 'images/image1606183153.jpeg', 'default.jpg', 1, 0, '2020-11-23 20:29:13', '2020-11-23 20:29:13'),
(3, 3, 'Aa', 'uploads/images/1606269035.jpeg', 'uploads/thumbnail/1606269035.jpeg', 1, 0, '2020-11-24 20:20:36', '2020-11-24 20:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `email_verified_at`, `phone_number`, `password`, `remember_token`, `is_deleted`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Kamren DuBuque I', 'ebert.charley@example.net', '2020-11-22 19:45:40', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bHQ0bpWXNf', 0, 1, '2020-11-22 19:45:40', '2020-11-22 19:45:40'),
(2, 'Boris Blanda', 'dale.wiza@example.net', '2020-11-22 19:45:40', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UlMk4fjz8k', 0, 1, '2020-11-22 19:45:40', '2020-11-22 19:45:40'),
(3, 'Ismael Goldner', 'dtremblay@example.net', '2020-11-22 19:45:40', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2IR3nmezXa', 0, 1, '2020-11-22 19:45:40', '2020-11-22 19:45:40'),
(4, 'Milford D\'Amore', 'vanessa.jacobson@example.org', '2020-11-22 19:45:40', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Eu9r5SYbSo', 0, 1, '2020-11-22 19:45:40', '2020-11-22 19:45:40'),
(5, 'Eugene Will', 'treutel.rodger@example.org', '2020-11-22 19:45:40', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '32EGVKLrVv', 0, 1, '2020-11-22 19:45:40', '2020-11-22 19:45:40'),
(6, 'Chaya Hill', 'macejkovic.bulah@example.net', '2020-11-22 20:03:59', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HIfXTXMERm', 0, 1, '2020-11-22 20:03:59', '2020-11-22 20:03:59'),
(7, 'Thea Lakin DVM', 'ava46@example.net', '2020-11-22 20:03:59', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hSzWUQTxWr', 0, 1, '2020-11-22 20:03:59', '2020-11-22 20:03:59'),
(8, 'Pasquale Legros', 'alda.kilback@example.net', '2020-11-22 20:03:59', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'whKbU6MxFA', 0, 1, '2020-11-22 20:03:59', '2020-11-22 20:03:59'),
(9, 'Granville Hermann', 'kirlin.unique@example.com', '2020-11-22 20:03:59', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CwDhYYmFVr', 0, 1, '2020-11-22 20:03:59', '2020-11-22 20:03:59'),
(10, 'Mrs. Precious Aufderhar IV', 'erdman.agustin@example.com', '2020-11-22 20:03:59', '1123122121', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dKLnNwg3cX', 0, 1, '2020-11-22 20:03:59', '2020-11-22 20:03:59'),
(11, 'Trilok Kumar', 'trilokkumar97@gmail.com', NULL, '7742223269', '$2y$10$UJgSF2mXxl0vWI9MaW38l.An71yXbJFeNYGiFrRjhP4OtQiIWbIAO', NULL, 0, 1, '2020-11-24 19:04:53', '2020-11-24 19:04:53'),
(12, 'Trilok Kumar', 'trilokkumar9@gmail.com', NULL, '7742223269', '$2y$10$0E91a0eTCL3ylyTp1tyxOOScPVykIDhPwdMcuVXyqJ6owN9islOoy', NULL, 0, 1, '2020-11-24 19:06:44', '2020-11-24 19:06:44'),
(13, 'Trilok Kumar', 'trilokkumar@gmail.com', NULL, '7742223269', '$2y$10$moWhzceHNWnlf5cV0S.aAOGziA/SScHkOzy9hBRqnM7ziS3FQl9tS', NULL, 0, 1, '2020-11-24 19:09:38', '2020-11-24 19:09:38'),
(14, 'Trilok Kumar', 'trilokkumar9701@gmail.com', NULL, '7742223269', '$2y$10$BmCBkq96EoBiwqXd1VXec.TJTJXZJ6Oga.Et7wZ9CGd8Cr9V5UF5W', NULL, 0, 1, '2020-11-24 19:12:44', '2020-11-24 19:12:44'),
(15, 'Trilok Kumar', 'trilokkumar9702@gmail.com', NULL, '7742223269', '$2y$10$7sshtkDVP2W7CK4f4SZEZ.4bEu2Ni3KJYW/X8/XSxZWiN1LWPeNlG', NULL, 0, 1, '2020-11-24 19:15:03', '2020-11-24 19:15:03'),
(16, 'Trilok Kumar', 'trilokkumar9703@gmail.com', NULL, '7742223269', '$2y$10$fqGWREHb5swmd3mvhqY6HuujTWgGimy9xyFz/XTluI/b3alAKjJ02', NULL, 0, 1, '2020-11-24 19:16:35', '2020-11-24 19:16:35'),
(17, 'Trilok Kumar', 'trilokkumar9704@gmail.com', NULL, '7742223269', '$2y$10$mUqD5qG.nqIQCEBbaOEqb.1Rs4o2Til6L32epHWual5wrpunJEUPK', NULL, 0, 1, '2020-11-24 19:17:32', '2020-11-24 19:17:32'),
(18, 'Trilok Kumar', 'trilokkumar970@gmail.com', NULL, '7742223269', '$2y$10$RBpTLMU8oJ17MoIvuq5E5ej6W.deOhRuz/6hGUjlhFFOTUNz8WKva', 'slLSchNWLY2kCqGAJCzILMUtTIxaLJA1luUFsL075EoCptZNZ56dkzU7BzGS3lg60oQErav3ttg3v7vFz8GDh70snHDlgjKcVbCZ', 0, 1, '2020-11-24 19:18:14', '2020-11-24 19:21:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_tokens`
--
ALTER TABLE `device_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `device_tokens`
--
ALTER TABLE `device_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
